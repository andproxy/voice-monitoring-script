#!/bin/bash

#####################################################
#                                                   #
#            Ping voice monitoring script           #
#      Добавляем ip или имена хостов через пробел.  #
#                                                   #
#      Add ip or host names separated by spaces.    #
#                                                   #
#####################################################

# What u need: install festival festvox-ru


##Hosts here
cisco="10.11.12.110 11.11.11.11 192.168.1.11"
dlink="10.120.120.12 21.21.21.21 192.168.2.25"
zyxel="10.30.30.130 31.31.31.31 192.168.3.54"
eltex="10.120.45.140 41.41.41.41 192.168.4.47"
servers="10.150.0.10 51.51.51.51 192.168.5.65"



###############################################
#                                             #
#   Рандомное целое число для random реплики  #
#  c добавлением новых фраз нужно увеличить   #
#  разброс от нуля до +количество добавленных #
#  фраз.                                      #
#                                             #
#  Random number for random replicas with     #
#  the addition of new phrases need to        #
#  increase the rank from zero to + the       #
#  number of added phrases.                   #
#                                             #
###############################################
random_roll=`echo "$((0 + RANDOM % 16))"`


###############################################
#                                             #
#           Словарь рендомных реплик          #
#                                             #
#           Rendome replica dictionary        #
#                                             #
###############################################
speech1="Но, это, не точно!"
speech2="Советую, проверить!"
speech3="Меня вообще слышно?"
speech4="Вам скоро позвонят?"
speech5="Восстание машин уже близко!"
speech6="Через 5 минут я это повторю!"
speech7="А в столовой сегодня макароны!"
speech8="Может быть перезагрузим?"
speech9="Выше нос, кусок мяса!"
speech10="Всё проверили, ничего, не работает!"
speech11="Опять вы всё сломали?"
speech12="Время пошло!"
speech13="Это не шутки!"
speech14="Кто, сломал?"
speech15="Крабы!"
speech16="Восстание машин уже началось!"

##############################################


################################################################
#                                                              #
#  		Произнесение фраз из словоря                   #
#                                                              #
#  c добавлением новых фраз нужно увеличить цифру сравнения    #
#  -gt на +количество добавленных фраз. И добавить новый elif  #
#  с новым порядковым номером фразы.                           #
#                                                              #
#                                                              #
#             Saying phrases from the dictionary               #
#                                                              #
#    If adding new phrases you need to increase the number     #
#    of comparison -gt for the + number of phrases added.      #
#    And add a new elif with a new phrase number.              #
#                                                              #
#                                                              #
################################################################
function random_speech {

random_roll2=`date +%S | cut -c 2-2`;

if [ $(($random_roll + $random_roll2)) -gt 16 ] ; then
        echo "$speech16" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 1 ] ; then
        echo "$speech1" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 2 ] ; then
        echo "$speech2" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 3 ] ; then
        echo "$speech3" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 4 ] ; then
        echo "$speech4" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 5 ] ; then
        echo "$speech5" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 6 ] ; then
        echo "$speech6" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 7 ] ; then
        echo "$speech7" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 8 ] ; then
        echo "$speech8" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 9 ] ; then
        echo "$speech9" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 10 ] ; then
        echo "$speech10" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 11 ] ; then
        echo "$speech11" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 12 ] ; then
        echo "$speech12" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 13 ] ; then
        echo "$speech13" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 14 ] ; then
        echo "$speech14" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 15 ] ; then
        echo "$speech15" | festival --tts --language russian
elif [ $(($random_roll + $random_roll2)) -eq 16 ] ; then
        echo "$speech16" | festival --tts --language russian
fi
}
#################################################################



#Реплика для cisco
#Phrase for cisco
function voice_cisco {
echo "Циско каталист не доступна!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
echo "Повторяю!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
random_speech
# Alert through Jabber, if you need...
#echo "[`date +%F/%H:%M:%S`] Cisco Catalyst ip address $mhost не доступен!" | sendxmpp username@jabber.com
}

#Реплика для dlink
#Phrase for dlink
function voice_dlink {
echo "Узел связи дэ линк не доступен!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
echo "Повторяю!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
random_speech
}

#Реплика для zyxel
#Phrase for zyxel
function voice_zyxel {
echo "Узел связи не доступен!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
echo "Повторяю!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
random_speech
}

#Реплика для eltex
#Phrase for eltex
function voice_eltex {
echo "Узел связи элтекс не доступен!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
echo "Повторяю!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
random_speech
}

#Реплика для servers
#Phrase for servers
function voice_servers {
echo "Сервер не доступен!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
echo "Повторяю!" | festival --tts --language russian
echo $mhost | sed -r -e 's/[.]/ /g' | festival --tts --language russian
random_speech
}


################################
#                              #
#       Количество пингов      #
#                              #
#          Ping count          #
#                              #
################################
COUNT=1
################################


#Основной скрипт
#В плане проверки можно проверить доступность хоста по snmp, например с помощью утилиты snmpwalk 
#Main script
#In terms of testing, you can check the availability of a host using snmp, for example using the snmpwalk utility

for mhost in $cisco
do
count=$(ping -c $COUNT $mhost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
if [ $count -eq 0 ]; then
echo $mhost | egrep "." > /dev/null
if [ $? -eq 0 ]; then
voice_cisco
fi
fi
done

for mhost in $dlink
do
count=$(ping -c $COUNT $mhost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
if [ $count -eq 0 ]; then
echo $mhost | egrep "." > /dev/null
if [ $? -eq 0 ]; then
voice_dlink
fi
fi
done

for mhost in $zyxel
do
count=$(ping -c $COUNT $mhost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
if [ $count -eq 0 ]; then
echo $mhost | egrep "." > /dev/null
if [ $? -eq 0 ]; then
voice_zyxel
fi
fi
done

for mhost in $eltex
do
count=$(ping -c $COUNT $mhost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
if [ $count -eq 0 ]; then
echo $mhost | egrep "." > /dev/null
if [ $? -eq 0 ]; then
voice_eltex
fi
fi
done

for mhost in $servers
do
count=$(ping -c $COUNT $mhost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
if [ $count -eq 0 ]; then
echo $mhost | egrep "." > /dev/null
if [ $? -eq 0 ]; then
voice_servers
fi
fi
done
